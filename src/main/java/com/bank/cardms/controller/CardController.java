package com.bank.cardms.controller;

import java.util.List;

import com.bank.cardms.model.Card;
import com.bank.cardms.model.Customer;
import com.bank.cardms.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("cards")
@RequiredArgsConstructor
public class CardController {

    private final CardRepository cardRepository;

    @PostMapping
    public List<Card> getCardDetails(@RequestBody Customer customer) {
        List<Card> card = cardRepository.findByCustomerId(customer.getCustomerId());
        if (card != null) {
            return card;
        } else {
            return null;
        }

    }

}