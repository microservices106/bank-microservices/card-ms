package com.bank.cardms.repository;

import java.util.List;

import com.bank.cardms.model.Card;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CardRepository extends CrudRepository<Card, Long> {


    List<Card> findByCustomerId(int customerId);

}